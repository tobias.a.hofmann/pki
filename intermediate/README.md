# Create intermediate CA

The root CA must be created first. After the root CA exists, you can start creating the intermediate CA.

The private key and public cert are stored in the directories:

* Private key: private/int.itsfullofstars.key.pem
* Public cert:

## Create private key

The private key is protected by a passphrase: abc123

The certificate will be issued to the subject:

* C=DE
* ST=BW
* L=Karlsruhe
* O=Demo
* CN=int.itsfullofstars.de

```sh
openssl req -config openssl_intermediate.cnf -passout pass:abc123 -new -newkey rsa:8192 -keyout private/int.itsfullofstars.key.pem -out ../rootca/csr/int.itsfullofstars.csr -subj "/C=DE/ST=BW/L=Karlsruhe/O=Demo/CN=int.itsfullofstars.de" 
```

## Create public cert

The signing request created above is signed by the root CA. Therefore, the following command is run in the rootca directory.

The passphrase for the root CA private key is privded (-passin pass:test123). The parameter -batch surpresses the openssl siging confirmation (y/N). The certificate is named int.itsfullofstars.crt.pem. As the cert is signed in the root CA directory, the intermediate CA cert is copied to the intermediate cert folder.

```sh
cd ../rootca
openssl ca -batch -config openssl_root.cnf -extensions v3_intermediate_ca -days 3650 -passin pass:test123 -notext -md sha512 -in csr/int.itsfullofstars.csr -out certs/int.itsfullofstars.crt.pem
cp certs/int.itsfullofstars.crt.pem ../intermediate/certs/
```

## (Optional) Create certificate chain

As a final step in the intermediate CA creation a certificate chain can be created. This is done be adding the root CA cert to the intermediate CA cert.

```sh
cat certs/int.itsfullofstars.crt.pem ../rootca/certs/ca.itsfullofstars.crt.pem > certs/chain.itsfullofstars.crt.pem
```

# Show X.509

```sh
openssl x509 -text -in certs/int.itsfullofstars.crt.pem 
```
