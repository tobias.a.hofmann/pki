# Create Root CA

The root CA consists of its private key and public cert.

* Private key: private/ca.itsfullofstars.key.pem
* Public cert: certs/ca.itsfullofstars.crt.pem 

## Create private key

Private key is protected by passphrase: test123

```sh
openssl genrsa -aes256 -passout pass:test123 -out private/ca.itsfullofstars.key.pem 8192
```

## Create Root CA certificate

For creating the cert, the passphrase for the private key (test123) is needed. The cert subject will be issued to: /C=DE/ST=BW/L=Karlsruhe/O=Demo/CN=ca.itsfullofstars.de" 

* C=DE
* ST=BW
* L=Karlsruhe
* O=Demo
* CN=ca.itsfullofstars.de

```sh
openssl req -config openssl_root.cnf -new -x509 -sha512 -extensions v3_ca -passin pass:test123 -key private/ca.itsfullofstars.key.pem -out certs/ca.itsfullofstars.crt.pem -days 3650 -set_serial 0 -subj "/C=DE/ST=BW/L=Karlsruhe/O=Demo/CN=ca.itsfullofstars.de" 
```

# Show X.509

```sh
openssl x509 -text -in certs/ca.itsfullofstars.crt.pem 
```