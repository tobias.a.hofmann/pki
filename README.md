# Public Key Infrasturcture with OpenSSL example

A sample PKI with OpenSSL that creates a root CA and intermediate CA.

# Installation

```sh
git clone https://gitlab.com/tobias.a.hofmann/pki
cd pki
```

# Create root CA

```sh
cd rootca
openssl genrsa -aes256 -passout pass:test123 -out private/ca.itsfullofstars.de.key.pem 8192
openssl req -config openssl_root.cnf -new -x509 -sha512 -extensions v3_ca -passin pass:test123 -key private/ca.itsfullofstars.de.key.pem -out certs/ca.itsfullofstars.de.crt.pem -days 3650 -set_serial 0 -subj "/C=DE/ST=BW/L=Karlsruhe/O=Demo/CN=ca.itsfullofstars.de" 
cd ..
```

# Create intermediate CA

```sh
cd intermediate/
openssl req -config openssl_intermediate.cnf -passout pass:abc123 -new -newkey rsa:8192 -keyout private/int.itsfullofstars.de.key.pem -out ../rootca/csr/int.itsfullofstars.de.csr -subj "/C=DE/ST=BW/L=Karlsruhe/O=Demo/CN=int.itsfullofstars.de"
cd ../rootca
openssl ca -batch -config openssl_root.cnf -extensions v3_intermediate_ca -days 3650 -passin pass:test123 -notext -md sha512 -in csr/int.itsfullofstars.de.csr -out certs/int.itsfullofstars.de.crt.pem
cp certs/int.itsfullofstars.de.crt.pem ../intermediate/certs/
cd ..
```

# Create certificate chain

```sh
cd intermediate
cat certs/int.itsfullofstars.de.crt.pem ../rootca/certs/ca.itsfullofstars.de.crt.pem > certs/chain.itsfullofstars.de.crt.pem
cd ..
```

# Create server certificate

A server certificate is created by creating a CSR and private key for the server. The csr is stored in the intermediate CA csr folder:

* intermediate/csr/blog.itsfullofstars.de.csr.pem

The private key is stored in a local private key folder.

* private/blog.itsfullofstars.de.key.pem

```sh
openssl req -out intermediate/csr/blog.itsfullofstars.de.csr.pem -newkey rsa:4096 -nodes -keyout private/blog.itsfullofstars.de.key.pem -config intermediate/openssl_intermediate.cnf -subj "/C=DE/ST=BW/L=Karlsruhe/O=Demo/CN=blog.itsfullofstars.de"
cd intermediate
openssl ca -batch -config openssl_intermediate.cnf -extensions server_cert -days 3050 -passin pass:abc123 -notext -md sha512 -in csr/blog.itsfullofstars.de.csr.pem -out certs/blog.itsfullofstars.de.crt.pem
cp certs/blog.itsfullofstars.de.crt.pem ../
cd ..
```

## Read server certificate

The subject of the new server certificate should read: 

Subject: C=DE, ST=BW, L=Karlsruhe, O=Demo, CN=blog.itsfullofstars.de

The extension should include that the X.509 is for a server and not for a CA.

```
X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Cert Type: 
                SSL Server
X509v3 Extended Key Usage: 
                TLS Web Server Authentication
```

```sh
openssl x509 -text -in blog.itsfullofstars.de.crt.pem 
```

As an alternative you can use keytool to inspect a X.509 cert.

```sh
keytool -printcert -file blog.itsfullofstars.de.pem 
```

# Sign a certificate request

In case you already do have a CSR request, you only need to let the intermediate CA sign it. As an example, assume you have a CSR as created by SAP Cloud Connector (scc_ui_csr.pem). Copy the csr file to intermediate/csr and sign it.

```sh
openssl ca -config intermediate/openssl_intermediate.cnf -extensions server_cert -days 3050 -notext -md sha512 -in intermediate/csr/scc_ui_csr.pem -out intermediate/certs/sapcc.itsfullofstars.de.crt.pem
```

# PEM to CRT

```sh
openssl x509 -outform der -in blog.itsfullofstars.de.crt.pem -out blog.itsfullofstars.de.crt
```

# Create PFX file

```sh
openssl pkcs12 -export -out blog.pfx -inkey private/blog.itsfullofstars.de.key.pem -in blog.itsfullofstars.de.crt.pem  -certfile rootca/certs/ca.itsfullofstars.de.crt.pem -certfile intermediate/certs/int.itsfullofstars.de.crt.pem
```